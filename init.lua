-- NOTE: jusqua's neovim config file, based on AstroNvim v4 template

-- NOTE: LAZY BOOTSTRAP SECTION

local lazypath = vim.env.LAZY or vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not (vim.env.LAZY or (vim.uv or vim.loop).fs_stat(lazypath)) then
  -- stylua: ignore
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(lazypath)

if not pcall(require, "lazy") then
  -- stylua: ignore
  vim.api.nvim_echo({ { ("Unable to load lazy from: %s\n"):format(lazypath), "ErrorMsg" }, { "Press any key to exit...", "MoreMsg" } }, true, {})
  vim.fn.getchar()
  vim.cmd.quit()
end

-- NOTE: PACKAGE CONFIG SECTION

---@type LazySpec
local astronvim = {
  "AstroNvim/AstroNvim",
  version = "^4",
  import = "astronvim.plugins",
}

---@type LazySpec
local astrocore = {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    features = {
      large_buf = { size = 1024 * 500, lines = 5000 },
      autopairs = true,
      cmp = true,
      diagnostics_mode = 3,
      highlighturl = true,
      notifications = false,
    },

    rooter = {
      autochdir = true,
    },

    diagnostics = {
      virtual_text = true,
      underline = true,
    },

    options = {
      opt = {
        signcolumn = "yes",
        relativenumber = true,
        number = true,
        spell = false,
        wrap = true,
        showtabline = 0,
      },
    },

    mappings = {
      n = {
        L = { function() require("astrocore.buffer").nav(vim.v.count1) end, desc = "Next buffer" },
        H = { function() require("astrocore.buffer").nav(-vim.v.count1) end, desc = "Previous buffer" },
        U = { "<cmd>redo<cr>", desc = "Undo" },
        J = { "mzJ`z", desc = "Join row" },
        d = { '"_d', desc = "Delete" },
        D = { '"_D', desc = "Delete forwardly row characters" },
        dd = { "Vx", desc = "Cut line" },
      },
      v = {
        d = { '"_d', desc = "Delete selected area" },
        p = { '"_dP', desc = "Paste" },
      },
    },
  },
}

---@type LazySpec
local astroui = {
  "AstroNvim/astroui",

  ---@type AstroUIOpts
  opts = {
    colorscheme = "catppuccin",

    icons = {
      TabClose = "",
      BufferClose = "",
    },
  },
}

---@type LazySpec
local astrocommunity = {
  "AstroNvim/astrocommunity",
  { import = "astrocommunity.pack.lua" },
  { import = "astrocommunity.pack.cpp" },
  { import = "astrocommunity.pack.lua" },
  { import = "astrocommunity.pack.python" },
  { import = "astrocommunity.pack.typescript" },
  { import = "astrocommunity.pack.tailwindcss" },
  { import = "astrocommunity.colorscheme.catppuccin" },
  { import = "astrocommunity.lsp.lsp-signature-nvim" },
  { import = "astrocommunity.motion.mini-move" },
  { import = "astrocommunity.startup.fsplash-nvim" },
  { import = "astrocommunity.recipes.disable-tabline" },
  { import = "astrocommunity.editing-support.rainbow-delimiters-nvim" },
  { import = "astrocommunity.editing-support.mini-splitjoin" },
  { import = "astrocommunity.editing-support.conform-nvim" },
  { import = "astrocommunity.editing-support.ultimate-autopair-nvim" },
}

---@type LazySpec
local heirline = {
  "rebelot/heirline.nvim",

  opts = function(_, opts)
    local status = require "astroui.status"

    opts.statusline = {
      hl = { fg = "fg", bg = "bg" },
      status.component.mode(),
      status.component.git_branch(),
      status.heirline.make_buflist(status.component.tabline_file_info()),
      status.component.fill(),
      status.component.cmd_info(),
      status.component.git_diff(),
      status.component.diagnostics(),
      status.component.lsp(),
      status.component.treesitter(),
      status.component.nav(),
      status.component.mode { surround = { separator = "right" } },
    }

    opts.winbar = nil

    return opts
  end,
}

-- NOTE: lazy.nvim plugin manager setup
require("lazy").setup({
  astronvim,
  astrocore,
  astroui,
  astrocommunity,

  heirline,
} --[[@as LazySpec]], {
  install = { colorscheme = { "catppuccin" } },
  ui = { backdrop = 100 },
  performance = {
    rtp = {
      disabled_plugins = {
        "gzip",
        "netrwPlugin",
        "tarPlugin",
        "tohtml",
        "zipPlugin",
      },
    },
  },
} --[[@as LazyConfig]])
