# jusqua's neovim config

"Onefile" config based on [AstroNvim](https://github.com/AstroNvim/AstroNvim) v4 template

![Screenshot](screenshot.png)

## Diff

- Preinstalled [astrocommunity](https://github.com/AstroNvim/astrocommunity) packages
- Custom [heirline.nvim](https://github.com/rebelot/heirline.nvim) using [astroui](https://github.com/AstroNvim/astroui) components
- Use [catppuccin](https://github.com/catppuccin/nvim) as default colorscheme
- Presetted keymap for delete, paste, yank and buffer switch

## Installation

### Backup (or remove) current config

```bash
mv ~/.config/nvim ~/.config/nvim.bak # rm ~/.config/nvim
mv ~/.local/share/nvim ~/.local/share/nvim.bak # rm ~/.local/share/nvim
mv ~/.local/state/nvim ~/.local/state/nvim.bak # rm ~/.local/state/nvim
mv ~/.cache/nvim ~/.cache/nvim.bak # rm ~/.cache/nvim
```

### Clone configuration

```bash
git clone https://gitlab.com/jusqua/nvim.git ~/.config/nvim
```

### Start Neovim

```bash
nvim
```
